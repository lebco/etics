library(dplyr)
library(readxl)
library(stringr)
library(tidyverse)
library(BiocManager)
bioc_packages <- c("phyloseq")
for (p in bioc_packages) {
  if (!require(p, character.only=TRUE)) BiocManager::install(p)
  library(p, character.only=TRUE)
}
setwd(".")
########################################################
######## PARTIE 1 = recupération de "last_taxo" ########
########################################################

#### Ouverture des données ####

# Objet Phyloseq
RAW_PHYLOSEQ_23S_NCBI<-readRDS("phyloseq_tax_consensus_microDecon_23S_NCBI.rds")
# Tax_table
tax_23S_NCBI<-tax_table(RAW_PHYLOSEQ_23S_NCBI) %>%
  as.data.frame()

#### Nettoyage taxonomie ####

tax_23S_NCBI <- tax_23S_NCBI %>%
  mutate(Species = str_remove(Species, ";"))

#### Verification des doublons d'espèces ayant une taxo différente ####

tax_23S_NCBI_unique_sp <-tax_23S_NCBI %>%
  filter(Species != "") %>%
  distinct()

length(tax_23S_NCBI_unique_sp$Species)
length(unique(tax_23S_NCBI_unique_sp$Species)) #les 2 ont la même longueur donc OK (ATTENTION --> garentie pas qu'il n'y ai pas d'erreur pour les taxons n'allant pas jusqu'à l'espèce)

#### Ajout de la colonne last_taxo ####
# (récupération du dernier rang taxo rempli)

#tavail sur le tableau tax_18S_W_unique car moins long

tax_23S_NCBI_unique<-tax_23S_NCBI %>%
  distinct()

tax_23S_NCBI_unique<-tax_23S_NCBI_unique %>%
  mutate_all(~na_if(., "")) %>% #remplace les cases vides par des NA
  mutate(index = colnames(.)[max.col(!is.na(.), ties.method = "last")]) %>% # Ajouter une colonne "index" contenant le nom de la dernière colonne remplie
  mutate(index = case_when( #On remplace dans la colonne index le noms de la dernière colonne remplie par un numéro
    index == "Species" ~ 7,
    index == "Genus" ~ 6,
    index == "Family" ~ 5,
    index == "Order" ~ 4,
    index == "Class" ~ 3,
    index == "Phylum" ~ 2,
    index == "Kingdom" ~ 1,
    TRUE ~ as.numeric(index)  # Si l'index n'est pas l'un des rangs taxonomiques connus, le conserver tel quel
  ))


tax_23S_NCBI_unique <- tax_23S_NCBI_unique%>%
  mutate(
    last_taxo = case_when(
      index == 7 ~ Species,
      index == 6 ~ Genus,
      index == 5 ~ Family,
      index == 4 ~ Order,
      index == 3 ~ Class,
      index == 2 ~ Phylum,
      index == 1 ~ Kingdom,
      TRUE ~ NA_character_
    )
  )

#### Ajout de la colonne last_taxo_modif ####
# ("correction" des valeurs de last taxo non_cohérentes)

tax_23S_NCBI_unique <- tax_23S_NCBI_unique %>%
  mutate(last_taxo_modif = last_taxo) %>%
  mutate(last_taxo_modif = ifelse(last_taxo == "Aglaothamnion sp. ARS-2011", "Aglaothamnion", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Anabaena cylindrica PCC 7122", "Anabaena cylindrica", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Apatococcus sp. S3F4-61", "Apatococcus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Bacillariophyceae sp. 1 AS-2014"|
                                  last_taxo == "Bacillariophyceae sp. 2 AS-2014", "Bacillariophyceae", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chaetoceros sp. 19 LG-2014", "Chaetoceros", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chlorella sp. CLEB", "Chlorella", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chlorophyceae sp. 3 AS-2014", "Chlorophyceae", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Closterium sp. FACHB-326", "Closterium", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Colaconema sp. ARS-2011", "Colaconema", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Coscinodiscus sp. 35 LG-2014", "Coscinodiscus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Cylindrospermum stagnale PCC 7417", "Cylindrospermum stagnale", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Dokdonia sp. 4H-3-7-5", "Dokdonia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Ectocarpales sp. 1 DWF-2013", "Ectocarpales", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Eunotia sp. 1 AS-2014", "Eunotia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Grammatophora sp. B LG-2014", "Grammatophora", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Klebsormidium sp. 1 AS-2014" |
                                  last_taxo == "Klebsormidium sp. 2 AS-2014",  "Klebsormidium", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Navicula sp. CCMP2566", "Navicula", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Nephroselmis sp. NJD-20", "Nephroselmis", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Nitzschia cf. ovalis"|
                                  last_taxo == "Nitzschia sp. 00196" |
                                  last_taxo == "Nitzschia sp. BOLD:AAO7110", "Nitzschia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Peyssonnelia sp. ARS-2011", "Peyssonnelia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Picosynechococcus [Leptolyngbya] sp. PCC 7376", "Picosynechococcus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Tetraselmis sp. SAI", "Tetraselmis", last_taxo_modif))

#### Extraction taxo pour worms ####
tax_worms_23S_NCBI<-tax_23S_NCBI_unique$last_taxo_modif %>%
  as.data.frame()

#write.csv(tax_worms_23S_NCBI, "Y:/ETICS/bioinfo/samba/results/etics_23S_microgreen_run1/02_report/NCBI_ASSIGN/16_blast/tax_worms_23S_NCBI.csv", row.names = FALSE)

#################################################################################################
######## PARTIE 2 = remplissage manuel de la taxonomie des taxon non reconnues par WORMS ########
#################################################################################################

#### Chargement nouvelle taxo ####

worms_tax_23S_NCBI<-read_excel("tax_worms_23s_ncbi_matched.xls")
colnames(worms_tax_23S_NCBI)<-c("ScientificName_data", "inconnu", "AphiaID", "Match_type", "Taxon_status", 
                             "ScientificName", "AphiaID_accepted", "ScientificName_accepted",  
                             "Kingdom", "Phylum", "Class", "Order", "Family", "Genus", "Subgenus", "Species", "Subspecies")
worms_tax_23S_NCBI<-worms_tax_23S_NCBI%>%
  slice (-1)%>%
  mutate(ScientificName_data = trimws(ScientificName_data, whitespace = "\"")) %>%
  select(ScientificName_data, Kingdom, Phylum, Class, Order, Family, Genus, Species) %>%
  distinct()

#### Modif taxo pas passée dans WORMS ####

worms_tax_23S_NCBI<-worms_tax_23S_NCBI %>%
  mutate(Kingdom = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Bacteria", Kingdom),
         Phylum = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Planctomycetes", Phylum),
         Class = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Planctomycetacia", Class),
         Order = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Planctomycetales", Order),
         Family = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Planctomycetaceae", Family),
         Genus = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "Rubinisphaera", Genus),
         Species = ifelse(ScientificName_data == "Rubinisphaera brasiliensis", "brasiliensis", Species),
         
         Kingdom = ifelse(ScientificName_data == "Fluviicola taffensis", "Bacteria", Kingdom),
         Phylum = ifelse(ScientificName_data == "Fluviicola taffensis", "Bacteroidetes", Phylum),
         Class = ifelse(ScientificName_data == "Fluviicola taffensis", "Flavobacteria", Class),
         Order = ifelse(ScientificName_data == "Fluviicola taffensis", "Flavobacteriales", Order),
         Family = ifelse(ScientificName_data == "Fluviicola taffensis", "Cryomorphaceae", Family),
         Genus = ifelse(ScientificName_data == "Fluviicola taffensis", "Fluviicola", Genus),
         Species = ifelse(ScientificName_data == "Fluviicola taffensis", "taffensis", Species),
         
         Kingdom = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Bacteria", Kingdom),
         Phylum = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Proteobacteria", Phylum),
         Class = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Betaproteobacteria", Class),
         Order = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Burkholderiales", Order),
         Family = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Burkholderiales incertae sedis", Family),
         Genus = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "Rubrivivax", Genus),
         Species = ifelse(ScientificName_data == "Rubrivivax gelatinosus", "gelatinosus", Species),
         
         Kingdom = ifelse(ScientificName_data == "Poa compressa", "Plantae", Kingdom),
         Phylum = ifelse(ScientificName_data == "Poa compressa", "Tracheophyta", Phylum),
         Class = ifelse(ScientificName_data == "Poa compressa", "Magnoliopsida", Class),
         Order = ifelse(ScientificName_data == "Poa compressa", "Poales", Order),
         Family = ifelse(ScientificName_data == "Poa compressa", "Poaceae", Family),
         Genus = ifelse(ScientificName_data == "Poa compressa", "Poa", Genus),
         Species = ifelse(ScientificName_data == "Poa compressa", "compressa", Species),
         
         Kingdom = ifelse(ScientificName_data == "Opisthokonta", "", Kingdom),
         Phylum = ifelse(ScientificName_data == "Opisthokonta", "", Phylum),
         Class = ifelse(ScientificName_data == "Opisthokonta", "", Class),
         Order = ifelse(ScientificName_data == "Opisthokonta", "", Order),
         Family = ifelse(ScientificName_data == "Opisthokonta", "", Family),
         Genus = ifelse(ScientificName_data == "Opisthokonta", "", Genus),
         Species = ifelse(ScientificName_data == "Opisthokonta", "", Species),
         
         Kingdom = ifelse(ScientificName_data == "Quercus alba", "Plantae", Kingdom),
         Phylum = ifelse(ScientificName_data == "Quercus alba", "Tracheophyta", Phylum),
         Class = ifelse(ScientificName_data == "Quercus alba", "Magnoliopsida", Class),
         Order = ifelse(ScientificName_data == "Quercus alba", "Fagales", Order),
         Family = ifelse(ScientificName_data == "Quercus alba", "Fagaceae", Family),
         Genus = ifelse(ScientificName_data == "Quercus alba", "Quercus", Genus),
         Species = ifelse(ScientificName_data == "Quercus alba", "alba", Species),
         
         Kingdom = ifelse(ScientificName_data == "Pinus banksiana", "Plantae", Kingdom),
         Phylum = ifelse(ScientificName_data == "Pinus banksiana", "Tracheophyta", Phylum),
         Class = ifelse(ScientificName_data == "Pinus banksiana", "Pinopsida", Class),
         Order = ifelse(ScientificName_data == "Pinus banksiana", "Pinales", Order),
         Family = ifelse(ScientificName_data == "Pinus banksiana", "Pinaceae", Family),
         Genus = ifelse(ScientificName_data == "Pinus banksiana", "Pinus", Genus),
         Species = ifelse(ScientificName_data == "Pinus banksiana", "banksiana", Species),
         
         Kingdom = ifelse(ScientificName_data == "Polydora", "Animalia", Kingdom),
         Phylum = ifelse(ScientificName_data == "Polydora", "Annelida", Phylum),
         Class = ifelse(ScientificName_data == "Polydora", "Polychaeta", Class),
         Order = ifelse(ScientificName_data == "Polydora", "Spionida", Order),
         Family = ifelse(ScientificName_data == "Polydora", "Spionidae", Family),
         Genus = ifelse(ScientificName_data == "Polydora", "Polydora", Genus),
         Species = ifelse(ScientificName_data == "Polydora", "", Species),
         
         Kingdom = ifelse(ScientificName_data == "Chaetocerotales", "Chromista", Kingdom),
         Phylum = ifelse(ScientificName_data == "Chaetocerotales", "Bacillariophyta", Phylum),
         Class = ifelse(ScientificName_data == "Chaetocerotales", "Bacillariophyceae", Class),
         Order = ifelse(ScientificName_data == "Chaetocerotales", "", Order),
         Family = ifelse(ScientificName_data == "Chaetocerotales", "", Family),
         Genus = ifelse(ScientificName_data == "Chaetocerotales", "", Genus),
         Species = ifelse(ScientificName_data == "Chaetocerotales", "", Species),
         
         Kingdom = ifelse(ScientificName_data == "Coscinodiscophyceae", "Chromista", Kingdom),
         Phylum = ifelse(ScientificName_data == "Coscinodiscophyceae", "Bacillariophyta", Phylum),
         Class = ifelse(ScientificName_data == "Coscinodiscophyceae", "Bacillariophyceae", Class),
         Order = ifelse(ScientificName_data == "Coscinodiscophyceae", "", Order),
         Family = ifelse(ScientificName_data == "Coscinodiscophyceae", "", Family),
         Genus = ifelse(ScientificName_data == "Coscinodiscophyceae", "", Genus),
         Species = ifelse(ScientificName_data == "Coscinodiscophyceae", "", Species))  %>%
  mutate_all(~na_if(., ""))

#write.csv(worms_tax_23S_NCBI, "Y:/ETICS/bioinfo/samba/results/etics_23S_microgreen_run1/02_report/MICROGREEN_ASSIGN/16_blast/Worms_taxo_complete_23S_NCBI.csv", row.names = FALSE)

##########################################################
######## PARTIE 3 = TRANSFORMATION OBJET PHYLOSEQ ########
##########################################################

#### Ouverture des données ####

# Objet Phyloseq
RAW_PHYLOSEQ_23S_NCBI<-readRDS("phyloseq_tax_consensus_microDecon_23S_NCBI.rds")
# Tax_table

tax_23S_NCBI<-tax_table(RAW_PHYLOSEQ_23S_NCBI) %>%
  as.data.frame()%>%
  mutate(Species = str_remove(Species, ";"))%>%
  mutate_all(~na_if(., "")) %>% #remplace les cases vides par des NA
  mutate(index = colnames(.)[max.col(!is.na(.), ties.method = "last")]) %>% # Ajouter une colonne "index" contenant le nom de la dernière colonne remplie
  mutate(index = case_when( #On remplace dans la colonne index le noms de la dernière colonne remplie par un numéro
    index == "Species" ~ 7,
    index == "Genus" ~ 6,
    index == "Family" ~ 5,
    index == "Order" ~ 4,
    index == "Class" ~ 3,
    index == "Phylum" ~ 2,
    index == "Kingdom" ~ 1,
    TRUE ~ as.numeric(index)  # Si l'index n'est pas l'un des rangs taxonomiques connus, le conserver tel quel
  ))%>%
  mutate(
    last_taxo = case_when(
      index == 7 ~ Species,
      index == 6 ~ Genus,
      index == 5 ~ Family,
      index == 4 ~ Order,
      index == 3 ~ Class,
      index == 2 ~ Phylum,
      index == 1 ~ Kingdom,
      TRUE ~ NA_character_
    )
  )%>%
  mutate(last_taxo_modif = last_taxo) %>%
  mutate(last_taxo_modif = ifelse(last_taxo == "Aglaothamnion sp. ARS-2011", "Aglaothamnion", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Anabaena cylindrica PCC 7122", "Anabaena cylindrica", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Apatococcus sp. S3F4-61", "Apatococcus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Bacillariophyceae sp. 1 AS-2014"|
                                    last_taxo == "Bacillariophyceae sp. 2 AS-2014", "Bacillariophyceae", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chaetoceros sp. 19 LG-2014", "Chaetoceros", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chlorella sp. CLEB", "Chlorella", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Chlorophyceae sp. 3 AS-2014", "Chlorophyceae", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Closterium sp. FACHB-326", "Closterium", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Colaconema sp. ARS-2011", "Colaconema", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Coscinodiscus sp. 35 LG-2014", "Coscinodiscus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Cylindrospermum stagnale PCC 7417", "Cylindrospermum stagnale", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Dokdonia sp. 4H-3-7-5", "Dokdonia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Ectocarpales sp. 1 DWF-2013", "Ectocarpales", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Eunotia sp. 1 AS-2014", "Eunotia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Grammatophora sp. B LG-2014", "Grammatophora", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Klebsormidium sp. 1 AS-2014" |
                                    last_taxo == "Klebsormidium sp. 2 AS-2014",  "Klebsormidium", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Navicula sp. CCMP2566", "Navicula", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Nephroselmis sp. NJD-20", "Nephroselmis", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Nitzschia cf. ovalis"|
                                    last_taxo == "Nitzschia sp. 00196" |
                                    last_taxo == "Nitzschia sp. BOLD:AAO7110", "Nitzschia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Peyssonnelia sp. ARS-2011", "Peyssonnelia", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Picosynechococcus [Leptolyngbya] sp. PCC 7376", "Picosynechococcus", last_taxo_modif),
         last_taxo_modif = ifelse(last_taxo == "Tetraselmis sp. SAI", "Tetraselmis", last_taxo_modif))%>%
  rownames_to_column(var = "ASV") %>%
  rename(nom_taxon = last_taxo_modif)%>%
  mutate_all(as.character) %>%
  mutate_all(~na_if(., "")) %>%
  select(ASV, nom_taxon)

liste_taxo_ref<-read.csv2("liste_taxon_commun_category.csv")

liste_taxo_ref<-liste_taxo_ref %>%
  rename(nom_taxon = ScientificName_data)%>%
  select(-X)

tax_23S_NCBI_new<-merge(tax_23S_NCBI, liste_taxo_ref, by = "nom_taxon", all.x = TRUE)
tax_23S_NCBI_new<-tax_23S_NCBI_new %>%
  column_to_rownames(var = "ASV") %>%
  as.matrix()

phyloseq_23S_NCBI<-RAW_PHYLOSEQ_23S_NCBI
tax_table(phyloseq_23S_NCBI)<-tax_23S_NCBI_new

#saveRDS(phyloseq_23S_NCBI, "Y:/ETICS/bioinfo/samba/results/etics_23S_microgreen_run1/02_report/NCBI_ASSIGN/phyloseq_tax_consensus_microDecon_worms.rds")

