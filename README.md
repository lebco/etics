# ETICS
Ce git contient les scripts utilisés lors de mon stage de M2. 

# Consensus 
Ces scripts font un premier consensus entre les 10 résultats obtenus par l’assignation taxonomique par blast. Un second consensus est fait entre le résultat obtenus par blast et celui de l’assignation taxonomique par le bayésien.
Chaque script est nommé « Consensus_gene_database/primer.r »
Ces scripts requièrent en entrée : 
-   taxonomy_assigned_23S_NCBI.tsv
-   blast_23S_NCBI_10hit.tsv
-   NCBI_23S_tax.tsv
-   phyloseq_23S_NCBI.rds
-   taxonomy_assigned_23S_microgreen.tsv
-   blast_23S_ microgreen _10hit.tsv
-   microgreen _ncbi_tax.tsv
-   phyloseq_23S_ microgreen.rds
-   taxonomy_assigned_18S_A.tsv
-   blast_18S_pr2_A_10hit.tsv
-   pr2_version_5.0.0_SSU_mothur.tax
-   phyloseq_18S_A.rds
-   taxonomy_assigned_18S_W.tsv
-   blast_18S_pr2_W_10hit.tsv
-   pr2_version_5.0.0_SSU_mothur.tax
-   phyloseq_18S_W.rds

Les fichiers de sortie sont : 
-   phyloseq_tax_consensus_23S_NCBI.rds
-   phyloseq_tax_consensus_23S_microgreen.rds
-   phyloseq_tax_consensus_18S_A.rds
-   phyloseq_tax_consensus_18S_W.rds

# Correction metadata
Ces scripts servent à corriger les erreurs dans les metadata des deux jeux de données 18S qui n’ont pas été corrigée avant d’avoir été passé dans SAMBA.
Les scripts sont nommés « script_correction_metadata_18S_primers.r »
Les fichiers d’entrée sont : 
-   phyloseq_tax_consensus_18S_A.rds
-   phyloseq_tax_consensus_18S_W.rds
Les fichiers de sortie sont : 
-   phyloseq_tax_consensus_corrected_metadata_18S_A.rds
-   phyloseq_tax_consensus_corrected_metadata_18S_W.rds

# MicroDecon
Ces scripts servent à éliminer les séquences de contaminants dans les échantillons qui peuvent l’être. Les feces sont décontaminés à partir de l’échantillon d’eau d’Argenton et l’eau Stérivex est décontaminé à partir du tampon d’extraction. 
Les scripts sont nommés : « microdecon_gene_database/primers.r »
Les fichiers d’entrée sont : 
-   phyloseq_tax_consensus_corrected_metadata_18S_A.rds
-   phyloseq_tax_consensus_corrected_metadata_18S_W.rds

Les fichiers de sortie sont : 
-   phyloseq_tax_consensus_microDecon_18S_A.rds
-   phyloseq_tax_consensus_microDecon_18S_W.rds

# Transformation taxonomie
Ces scripts servent à extraire la taxonomie obtenue avec les bases de données utilisées (NCBI trimmed, µgreen-db, PR2) (ETAPE1), à remplir manuellement la taxonomie des séquences pour lesquelles worms n’a pas trouvé de correspondance (ETAPE2) et a transformé l’objet phyloseq final (ETAPE 3).
Les scripts sont nommés : « transfo_taxo_gene_database/primers.r »
Les fichiers d’entrée sont : 
-   phyloseq_tax_consensus_microDecon_23S_microgreen.rds
-   tax_worms_23S_microgreen _matched.xls
-   phyloseq_tax_consensus_microDecon_23S_NCBI.rds
-   tax_worms_23S_NCBI_matched.xls
-   phyloseq_tax_consensus_microDecon_18S_W.rds
-   tax_worms_18s_W_matched.xls
-   phyloseq_tax_consensus_microDecon_18S_A.rds
-   tax_worms_18s_a_matched.xls
-   liste_taxon_commun_category.csv
Les fichiers de sortie sont :
-   tax_worms_23S_microgreen.csv
-   Worms_taxo_complete_23S_microgreen.csv
-   phyloseq_tax_consensus_microDecon_worms_23S_microgreen.rds
-   tax_worms_23S_NCBI.csv
-   Worms_taxo_complete_23S_NCBI.csv
-   phyloseq_tax_consensus_microDecon_worms_23S_NCBI.rds
-   tax_worms_18S_W.csv
-   Worms_taxo_complete_18S_W.csv
-   phyloseq_tax_consensus_microDecon_worms_18S_W.rds
-   tax_worms_18S_A.csv
-   Worms_taxo_complete_18S_A.csv
-   phyloseq_tax_consensus_microDecon_worms_18S_A.rds

# Création d’une table regroupant toutes taxonomie et attribution des catégories 
Ce script sert a regroupé les taxonomies uniques obtenues avec le 23S_NCBI, le 23S_microgreen, le 18S_A et le 18S_W et a assigné chacune de ces taxonomies à une des 13 catégories crées (Algae, Macroalgae, Dinophyceae, Bacillariophyceae, Cyanobacteria, Other_phytoplankton, Fungi, Ciliates, Other_protists, Bacteria, Animalia, Terrestrial_plantae, Others)
Le script s’appelle : « Creation_table_ref_et_categories.r »
Les fichiers d’entrée sont : 
-   Worms_taxo_complete_18S_A.csv
-   Worms_taxo_complete_18S_W.csv
-   Worms_taxo_complete_23S_microgreen.csv
-   Worms_taxo_complete_23S_NCBI.csv
Le fichier de sortie est :
-   liste_taxon_commun_category.csv

# Analyses statistiques et représentation de données
Ce script sert à la représentation et à l’analyse des données (alpha diversité, beta diversité, électivité, …) 
Ce script s’appelle « script_stage_M2_final_V4.rmd »
Les fichiers d’entrée sont :
-   phyloseq_tax_consensus_microDecon_worms_23S_microgreen.rds
-   phyloseq_tax_consensus_microDecon_worms_23S_NCBI.rds
-   phyloseq_tax_consensus_microDecon_worms_18S_A.rds
-   phyloseq_tax_consensus_microDecon_worms_18S_W.rds
-   liste_taxon_commun_category.csv
-   flore_microscope_modif.csv (liste des taxons trouvés en microscopie dans l’eau)
