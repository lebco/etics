## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ####
##                                                                            ## 
## Script name: double_valid_tax_assign.R                                   ####
##                                                                            ##
## Purpose of script: Perform a double comparaison of taxonomic assignation ####
##                    to arrive at an accurate consensus                    ####
##                                                                            ##
## Authors: Cyril Noël (PhD) - SeBiMER                                      ####
##                                                                            ##
## Date Created: 2024-02-09                                                 ####
##                                                                            ##
## Copyright (c) Cyril Noël, 2024                                             ##
##                                                                            ##
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ####

#### 0) Set working directory and load  R packages ####
setwd(".")
required_packages <- c("dplyr", "stringr")
for(p in required_packages) {
  if(!require(p, character.only=T)) install.packages(p, character.only=T)
  library(p, character.only=T)
}

#### 1) Import raw NB result ####
NB <- read.table("taxonomy_assigned_18S_W.tsv", sep="\t")
colnames(NB) <- c("ASV_ID", "Taxon" , "Confidence")

#### 2) Import raw BLAST result ####
raw_BLAST <- read.table("blast_18S_pr2_W_10hit.tsv", sep="\t")
reduced_BLAST <- raw_BLAST[,c(1:3,5)]
colnames(reduced_BLAST) <- c("ASV_ID", "Ref_ID" , "Perc_similarity", "Coverage")

#### 3) Retrieve taxonomy of ref from BLAST result ####
ref_tax <- read.table("pr2_version_5.0.0_SSU_mothur.tax", sep="\t", quote="")
colnames(ref_tax) <- c("Ref_ID" , "Taxon")
reduced_BLAST_TAX <- merge(reduced_BLAST, ref_tax, by="Ref_ID")

#### 4) Merge NB and BLAST results ####
BLAST_NB_ALL <- merge(reduced_BLAST_TAX, NB, by="ASV_ID", all=T)
colnames(BLAST_NB_ALL) <- c("ASV_ID", "Ref_ID", "Perc_similarity", "Coverage", "Taxonomy_BLAST", "Taxonomy_NB", "Confidence")
#write.table(BLAST_NB_ALL, "compar_table_BLAST_NB_ALL.tsv", col.names=T, row.names=F, sep="\t", quote=F)

#### 5) Merge NB and with filtered BLAST results ####
#' * BLASTn result filter: *
#' *   - % identity > 95   *
#' *   - % coverage > 90   *
filtered_BLAST_TAX <- reduced_BLAST_TAX[reduced_BLAST_TAX$Perc_similarity > 95 & reduced_BLAST_TAX$Coverage > 90,]
BLAST_NB_FILTERED <- merge(filtered_BLAST_TAX, NB, by="ASV_ID", all=T)
colnames(BLAST_NB_FILTERED) <- c("ASV_ID", "Ref_ID", "Perc_similarity", "Coverage", "Taxonomy_BLAST", "Taxonomy_NB", "Confidence")
#write.table(BLAST_NB_FILTERED, "compar_table_BLAST_NB_FILTERED.tsv", col.names=T, row.names=F, sep="\t", quote=F)

#### 6) Find consensus tax assignation ####
#' * Creating the consensus taxonomy table *

CONSENSUS_TAX <- BLAST_NB_FILTERED %>% 
  group_by(ASV_ID) %>% 
  reframe(Taxonomy_NB, Confidence, across(Taxonomy_BLAST, ~trimws(paste0(.x, collapse=",")))) 
CONSENSUS_TAX <- CONSENSUS_TAX[!duplicated(CONSENSUS_TAX), ]
CONSENSUS_TAX <- data.frame(CONSENSUS_TAX$ASV_ID, CONSENSUS_TAX$Taxonomy_NB, CONSENSUS_TAX$Confidence, do.call(rbind, list(str_split_fixed(CONSENSUS_TAX$Taxonomy_BLAST, ",", 10))))
colnames(CONSENSUS_TAX) <- c("ASV_ID", "Taxonomy_NB", "Confidence", "Taxonomy_BLAST_hit1", "Taxonomy_BLAST_hit2", "Taxonomy_BLAST_hit3", "Taxonomy_BLAST_hit4", "Taxonomy_BLAST_hit5", "Taxonomy_BLAST_hit6", "Taxonomy_BLAST_hit7", "Taxonomy_BLAST_hit8", "Taxonomy_BLAST_hit9", "Taxonomy_BLAST_hit10")
CONSENSUS_TAX[CONSENSUS_TAX == ""] <- NA
CONSENSUS_TAX[CONSENSUS_TAX == "NA"] <- NA
BLAST_MATCH_TEST <- CONSENSUS_TAX %>% # on teste si les resultats de blast sont identiques entre eux
  select("Taxonomy_BLAST_hit1", "Taxonomy_BLAST_hit2", "Taxonomy_BLAST_hit3", "Taxonomy_BLAST_hit4", "Taxonomy_BLAST_hit5", "Taxonomy_BLAST_hit6", "Taxonomy_BLAST_hit7", "Taxonomy_BLAST_hit8", "Taxonomy_BLAST_hit9", "Taxonomy_BLAST_hit10") %>% 
  rowwise %>% 
  mutate(BLAST_match = n_distinct(na.omit(unlist(cur_data()))) == 1) %>% # crée une colonne qui dit TRUE si tous les résultats du blast sont les mêmes et FALSE si ce n'est pas le cas
  ungroup()
ASSIGN_MATCH_TEST <- CONSENSUS_TAX %>% # on teste si les résultats de blaste et de bayesien sont identiques entre eux
  select("Taxonomy_NB", "Taxonomy_BLAST_hit1", "Taxonomy_BLAST_hit2", "Taxonomy_BLAST_hit3", "Taxonomy_BLAST_hit4", "Taxonomy_BLAST_hit5", "Taxonomy_BLAST_hit6", "Taxonomy_BLAST_hit7", "Taxonomy_BLAST_hit8", "Taxonomy_BLAST_hit9", "Taxonomy_BLAST_hit10") %>% 
  rowwise %>% 
  mutate(all_assign_match = n_distinct(na.omit(unlist(cur_data()))) == 1) %>% 
  ungroup()
CONSENSUS_TAX <- cbind(CONSENSUS_TAX, BLAST_MATCH_TEST$BLAST_match, ASSIGN_MATCH_TEST$all_assign_match)
colnames(CONSENSUS_TAX) <- c("ASV_ID", "Taxonomy_NB", "Confidence", "Taxonomy_BLAST_hit1", "Taxonomy_BLAST_hit2", "Taxonomy_BLAST_hit3", "Taxonomy_BLAST_hit4", "Taxonomy_BLAST_hit5", "Taxonomy_BLAST_hit6", "Taxonomy_BLAST_hit7", "Taxonomy_BLAST_hit8", "Taxonomy_BLAST_hit9", "Taxonomy_BLAST_hit10", "BLAST_match", "all_assign_match")
CONSENSUS_TAX$BLAST_COMMON_TAX <- "" 
for(row in 1:nrow(CONSENSUS_TAX)) { #On cherche les niveaux taxonomiques communs parmis les résultats de blast et on les met dans la colonne BLAST_COMMON_TAX
  tmp_blast <- rbind(CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit1,CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit2, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit3, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit4, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit5, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit6, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit7,CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit8, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit9, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit10)
  tmp_blast_taxrank <- str_split_fixed(tmp_blast, ";", 9)
  if(length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,9]!="", 9]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:9], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,8]!="", 8]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:8], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,7]!="", 7]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:7], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,6]!="", 6]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:6], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,5]!="", 5]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:5], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,4]!="", 4]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:4], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,3]!="", 3]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:3], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,2]!="", 2]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:2], collapse=";")
  } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,1]!="", 1]))) == 1) {
    CONSENSUS_TAX[row,]$BLAST_COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:1], collapse=";")
  }
}
CONSENSUS_TAX$COMMON_TAX <- ""
for(row in 1:nrow(CONSENSUS_TAX)) { # On cherche la taxonomie commune entre blast et bayesien 
  tmp_blast <- rbind(CONSENSUS_TAX[row,]$Taxonomy_NB,CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit1,CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit2, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit3, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit4, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit5, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit6, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit7,CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit8, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit9, CONSENSUS_TAX[row,]$Taxonomy_BLAST_hit10)
  tmp_blast_taxrank <- str_split_fixed(tmp_blast, ";", 9)
  tmp_data_type <- data.frame(table(is.na(tmp_blast_taxrank[,1])))
  if (dim(tmp_data_type)[1] == 1) {
    if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,9]!="", 9]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:9], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,8]!="", 8]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:8], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,7]!="", 7]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:7], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,6]!="", 6]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:6], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,5]!="", 5]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:5], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,4]!="", 4]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:4], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,3]!="", 3]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:3], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,2]!="", 2]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:2], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,1]!="", 1]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:1], collapse=";")
    }
  } else {
    if (tmp_data_type[2,2] == 10) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:1], collapse=";") 
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,9]!="", 9]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:9], collapse=";") 
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,8]!="", 8]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:8], collapse=";") 
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,7]!="", 7]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:7], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,6]!="", 6]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:6], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,5]!="", 5]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:5], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,4]!="", 4]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:4], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,3]!="", 3]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:3], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,2]!="", 2]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:2], collapse=";")
    } else if (length(unique(na.omit(tmp_blast_taxrank[tmp_blast_taxrank[,1]!="", 1]))) == 1) {
      CONSENSUS_TAX[row,]$COMMON_TAX <- paste(str_split_fixed(tmp_blast, ";", 9)[1,1:1], collapse=";")
    }
  }
}
CONSENSUS_TAX$BLAST_COMMON_TAX <- str_replace_all(CONSENSUS_TAX$BLAST_COMMON_TAX, "NA;;;;;;", "")
CONSENSUS_TAX[CONSENSUS_TAX == ""] <- NA
CONSENSUS_TAX$Taxonomy_NB_GENUS <- sapply(strsplit(CONSENSUS_TAX$Taxonomy_NB, ";", fixed=TRUE), function(x) paste(head(x, -1), collapse=";"))
CONSENSUS_TAX[is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit1) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit2) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit3) & 
                is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit4) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit5) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit6) & 
                is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit7) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit8) & is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit9) & 
                is.na(CONSENSUS_TAX$Taxonomy_BLAST_hit10),]$BLAST_match <- NA
CONSENSUS_TAX[is.na(CONSENSUS_TAX$BLAST_match),]$all_assign_match <- FALSE
#write.table(CONSENSUS_TAX, "compar_table_BLAST_NB_FILTERED_wide.tsv", col.names=T, row.names=F, sep="\t", quote=F)

CONSENSUS_TAX <- CONSENSUS_TAX %>% mutate(Consensus_taxonomy = case_when(all_assign_match == TRUE ~ Taxonomy_NB, # Qd taxo du blast et du bayesien sont identique on prend la taxo du bayesien (eput importe puisque sont identiques)
                                                                         BLAST_match == TRUE & all_assign_match == FALSE & Confidence <= 0.5 ~ BLAST_COMMON_TAX, # Qd tt blast identiques mais différentes du bayésien et que score bootstrap bayésien < 0,5 on prd taxo commune aux blast
                                                                         BLAST_match == TRUE & all_assign_match == FALSE & Confidence > 0.5 ~ COMMON_TAX, # Qd tt blast identiques mais différentes du bayésien et que score bootstrap bayésien > 0,5 on prd taxo commune entre blast et bayesien
                                                                         all_assign_match == FALSE & BLAST_match == FALSE ~ COMMON_TAX, #Qd blast différents entre eux et différents de bayésien on prd taxo commune entre blast et bayesien
                                                                         is.na(BLAST_match) & all_assign_match == FALSE & Confidence > 0.5 ~ Taxonomy_NB, #Si pas de résultats de blast et que bootstrap bayesien > 0,5 on prd taxo du bayesien
                                                                         is.na(BLAST_match) & all_assign_match == FALSE & Confidence <= 0.5 & Confidence > 0.1 ~ Taxonomy_NB_GENUS, #Si pas de résultats de blast et que bootstrap bayesien <> °,1 et 0,5 on prd taxo du bayesien au niveau du genre
                                                                         is.na(BLAST_match) & all_assign_match == FALSE & Confidence <= 0.1 ~ COMMON_TAX)) #Si pas de résultats de blast et que bootstrap bayesien < 0,1 on prd taxo commune (unassigned)
#write.table(CONSENSUS_TAX, "compar_table_BLAST_NB_FILTERED_consensus.tsv", col.names=T, row.names=F, sep="\t", quote=F)
FINAL_CONSENSUS_TAX <- CONSENSUS_TAX[,c("ASV_ID", "Consensus_taxonomy")]
#write.table(FINAL_CONSENSUS_TAX, "consensus_tax_table.tsv", col.names=T, row.names=F, sep="\t", quote=F)

#### 7) Reload consensus tax in raw phyloseq ####

RAW_PHYLOSEQ <- readRDS("phyloseq_18S_W.rds")
PHYLOSEQ_TAX_CONSENSUS <- RAW_PHYLOSEQ

FINAL_CONSENSUS_TAX_reformat <- FINAL_CONSENSUS_TAX
FINAL_CONSENSUS_TAX_reformat <- data.frame(FINAL_CONSENSUS_TAX_reformat$ASV_ID, 
                                           do.call(rbind,list(str_split_fixed(FINAL_CONSENSUS_TAX_reformat$Consensus_taxonomy, ";",9))))
rownames(FINAL_CONSENSUS_TAX_reformat) <- FINAL_CONSENSUS_TAX_reformat$FINAL_CONSENSUS_TAX_reformat.ASV_ID
FINAL_CONSENSUS_TAX_reformat <- FINAL_CONSENSUS_TAX_reformat %>% select(-FINAL_CONSENSUS_TAX_reformat.ASV_ID)
colnames(FINAL_CONSENSUS_TAX_reformat) <- c("Kingdom", "Supergroup", "Division", "Subdivision", "Class", "Order", "Family", "Genus", "Species")

tax_table(PHYLOSEQ_TAX_CONSENSUS) <- tax_table(as.matrix(FINAL_CONSENSUS_TAX_reformat))
#saveRDS(PHYLOSEQ_TAX_CONSENSUS, "phyloseq_tax_consensus.rds")
